package com.moola.dto;


import com.moola.model.Note;

/**
 * Created by temp on 11/20/2015.
 */
public class NoteDTO {
    Long id;
    String title;
    String body;

    public NoteDTO() {
    }

    public NoteDTO(Note note) {
        this.title = note.getTitle();
        this.body = note.getBody();
        this.id = note.getId();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
