package com.moola.config;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import java.io.File;

/**
 * Created by temp on 11/29/2015.
 */
@Configuration
public class ApplicationConfig {
    @Bean
    public PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
        final PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
        Resource[] properties = new Resource[]{
                new ClassPathResource("/appilication.properties"),
                new FileSystemResource("/srv/config/application.properties"),
                new FileSystemResource("e:/srv/config/application.properties"),
        };

        ppc.setIgnoreResourceNotFound(true);
        ppc.setSearchSystemEnvironment(true);
        ppc.setSystemPropertiesMode(PropertyPlaceholderConfigurer.SYSTEM_PROPERTIES_MODE_OVERRIDE);
        ppc.setLocations(properties);

        return ppc;
    }
}
