package com.moola.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

import javax.sql.DataSource;

/**
 * Created with IntelliJ IDEA.
 * User: tienhd
 * Date: 4/11/14
 * Time: 1:54 PM
 */
@Configuration
@EnableTransactionManagement
public class JpaRepositoryConfig implements TransactionManagementConfigurer {
// ------------------------------ FIELDS ------------------------------

    @Value("${spring.database.driverClassName}")
    private String driver;
    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;
    @Value("${hibernate.dialect}")
    private String dialect;
    @Value("${spring.jpa.hibernate.ddl-auto}")
    private String hbm2ddlAuto;


// --------------------- Interface TransactionManagementConfigurer ---------------------

    @Override
    @Bean
    public PlatformTransactionManager annotationDrivenTransactionManager() {
        return new JpaTransactionManager();
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        return annotationDrivenTransactionManager();
    }

//    @Bean
//    public PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
//        final PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
//        Resource[] properties = new Resource[]{
//                new ClassPathResource("/appilication.properties"),
//                new FileSystemResource("/srv/config/application.properties"),
//                new FileSystemResource("e:/srv/config/application.properties"),
//        };
//
//        ppc.setIgnoreResourceNotFound(true);
//        ppc.setSearchSystemEnvironment(true);
//        ppc.setSystemPropertiesMode(PropertyPlaceholderConfigurer.SYSTEM_PROPERTIES_MODE_OVERRIDE);
//        ppc.setLocations(properties);
//
//        return ppc;
//    }


    // -------------
    @Bean
    public DataSource dataSource() {
        System.out.println("-1111--");
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
//    dataSource.setP
        return dataSource;
    }

}
