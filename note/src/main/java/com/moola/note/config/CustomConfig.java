package com.moola.note.config;

import com.gem.dao.NoteDAO;
import com.gem.dao.impl.NoteDAOImpl;
import javafx.application.Application;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.web.servlet.mvc.Controller;

/**
 * Created by temp on 11/20/2015.
 */
@Configuration
@ComponentScan(basePackageClasses = Application.class)
public class CustomConfig
{

    @Bean
    @Scope("singleton")
    public NoteDAO noteDAO()
    {
        return new NoteDAOImpl();
    }
}
