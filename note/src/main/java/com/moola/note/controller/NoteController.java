/*
 * Copyright 2012-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.moola.note.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.moola.dto.NoteDTO;
import com.moola.note.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
public class NoteController {
    @Autowired
    private NoteService noteService;

    @RequestMapping(value = "/get", method = RequestMethod.GET, consumes = {MediaType.ALL_VALUE})
    @ResponseBody

    public NoteDTO get(@RequestParam @Named("id") Long id, HttpServletRequest request) {
        System.out.println(request.getAttribute("userId"));
        return noteService.findOne(id);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = {MediaType.ALL_VALUE
    })
    @ResponseBody
    public NoteDTO add(@RequestBody @Named("noteDTO") NoteDTO noteDTO) {
        return noteService.add(noteDTO);
    }

    @RequestMapping(value = "/addInParam", method = RequestMethod.POST, consumes = {MediaType.ALL_VALUE
    })
    @ResponseBody
    public NoteDTO addInParam(@RequestParam String noteDTOString, @RequestParam String param1) {
        ObjectMapper mapper = new ObjectMapper();
        NoteDTO noteDTO = null;
        try {
            noteDTO = mapper.readValue(noteDTOString, NoteDTO.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(param1);
        return noteService.add(noteDTO);
    }
}
