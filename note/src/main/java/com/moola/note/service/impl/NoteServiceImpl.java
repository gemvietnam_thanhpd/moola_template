package com.moola.note.service.impl;

import com.moola.dao.NoteDAO;
import com.moola.dto.NoteDTO;
import com.moola.model.Note;
import com.moola.note.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by temp on 11/20/2015.
 */
@Component
//@Service
@Transactional
public class NoteServiceImpl implements NoteService {
    @Autowired
    private NoteDAO noteDAO;

    @Override
    public List<NoteDTO> findAll() {
        List<Note> notes = this.noteDAO.findAll();
        List<NoteDTO> noteDTOList = new ArrayList<NoteDTO>();
        for (Note note : notes) {
            NoteDTO noteDTO = new NoteDTO(note);
            noteDTOList.add(noteDTO);
        }

        return noteDTOList;
    }

    @Override
    public NoteDTO findOne(Long id) {
        return new NoteDTO(noteDAO.findOne(id));
    }

    @Override
    public NoteDTO add(NoteDTO noteDTO) {
        Note note = new Note();
        note.setTitle(noteDTO.getTitle());
        note.setBody(noteDTO.getBody());

        noteDAO.save(note);

        return new NoteDTO(note);
    }
}
