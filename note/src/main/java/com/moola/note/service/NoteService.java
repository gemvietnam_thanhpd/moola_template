package com.moola.note.service;

import com.moola.dto.NoteDTO;

import java.util.List;

/**
 * Created by temp on 11/20/2015.
 */
public interface NoteService
{
    List<NoteDTO> findAll();

    NoteDTO findOne(Long id);

    NoteDTO add(NoteDTO noteDTO);
}
