package com.moola;

import java.io.*;

/**
 * Created by temp on 11/29/2015.
 */
public class TestNote {
    public static void main(String[] args) throws IOException {


//        BufferedReader br = new BufferedReader(new FileReader("E:\\srv\\config\\test.txt"));
        BufferedReader br = new BufferedReader(new FileReader("\\srv\\config\\application.properties"));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                System.out.println(line);
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            String everything = sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            br.close();
        }

    }
}
